/// Generate the nth Fibonacci number.

pub fn generate_the_nth_fibonacci_number() {
    let mut n = String::new();

    println!("input the n for fibonacci count(should be positive):");
    
    std::io::stdin()
    .read_line(&mut n)
    .expect("Failed read line fibonacci count.");

    let n :u32 = match n.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            let n = n.trim();
            panic!("{n} can't to transform to u32.");
        },
    };

    let result = nth_fibonacci_match(n);
    println!("the {n}th fibonacci number is {result}.");
}

pub fn bad_generate_the_nth_fibonacci_number(n: u32) {
    let mut count: u32 = 1;
    let mut number: u32;
    let mut pre_number: u32 = 0;
    let mut two_pre_number: u32 = 0;

    while count <= n {
        if count < 2 {
            number = 0;
        } else if count == 2 {
            number = 1;
        } else {
            number = two_pre_number + pre_number;
        }
        two_pre_number = pre_number;
        pre_number = number;

        println!("{number}");
        count += 1;
    }
    println!("Finish.");
}

pub fn nth_fibonacci_match(n: u32) -> u32 {
    let result = match n {
        0 | 1 => 1,
        _ => nth_fibonacci_match(n-1) + nth_fibonacci_match(n-2),
    };
    return result;
}

pub fn nth_fibonacci_ifelse(n: u32) -> u32 {
    let result: u32;

    if n <= 2 {
        result = n;
    } else {
        result = nth_fibonacci_ifelse(n-1) + nth_fibonacci_ifelse(n-2);
    }
    return result;
}