/// Convert temperatures between Fahrenheit and Celsius.

pub fn convert_temperatures_between_fahrenheit_and_celsius() {
    println!("input the temperature type(C or F):");

    let mut temp_type = String::new();

    std::io::stdin()
    .read_line(&mut temp_type)
    .expect("Faild read line temp type.");

    let mut temp_type = match temp_type
    .trim()
    .to_lowercase()
    .as_str() {
        "c" => "c",
        "f" => "f",
        _ => {
            let temp_type: &str = temp_type.trim();
            panic!("bad input temperature type! \"{temp_type}\" should be \"c/C\" or \"f/F\".");
        },
    };
    println!("the temperature's type is: {temp_type}.");

    println!("input the temperature value(float-point number):");

    let mut temp = String::new();
    
    std::io::stdin()
    .read_line(&mut temp)
    .expect("Faild read line temp.");

    let mut temp: f32 = match temp.trim().parse() {
        Ok(float) => float,
        Err(_) => {
            let temp = temp.trim();
            panic!("bad input temperature value! \"{temp}\" should be float-point number.");
        },
    };
    println!("the temperature's value is: {temp}.");

    if temp_type.to_lowercase() == "c" {
        temp_type = "\u{2109}";  // F type return

        temp = 9.0 / 5.0 + 32.0;
    } else if temp_type.to_lowercase() == "f" {
        temp_type = "\u{2103}";  // C type return

        temp = 5.0 / 9.0 * (temp - 32.0);
    } else {
        panic!("calculate error!");
    };

    println!("{}{}", temp, temp_type);
}
