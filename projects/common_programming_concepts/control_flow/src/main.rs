// Control Flow
// `if` expressions and loops

// if Expressions

use std::ops::Index;

fn main() {
    let number = 3;

    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
}
// Blocks of code associated with the condition in
// `if` expressions are sometimes called *arms*.

// the condition must be a `bool`, if not, get error.
/*
fn condition_not_bool_main() {
    let number = 3;

    if number {
        println!("number was three");
    }
}
*/
// error[E0308]: mismatched types
// correct if expression:
fn correct_condition_bool_main() {
    let number = 3;

    if number != 0 {
        println!("number was something other than zero");
    }
}


// Handing Multiple Conditions with else if

// can use multiple conditions by combining `if` and `else` in an `else if` expression:
fn combining_condition_main() {
    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
}
// it checks each `if` expression in turn and executes the *first body* for which the condition evaluates to `true`.
// and once it finds one, it doesn’t even check the rest.
// so output is 
// number is divisible by 3


// Using if in let statement

// Because `if` is an expression,
// can use it on the right side of a let statement,
// to assign the outcome to a variable:
fn if_in_let_statement_main() {
    let condition = true;
    let number = if condition { 5 } else { 6 };

    println!("The value of number is: {number}");
}

// the results of `if` arm and `else` arm must be same type.
// if not, get error:
/*
fn arm_results_type_different_main() {
    let condition = true;

    let number = if condition { 5 } else { "six" };

    println!("The value of number is: {number}");
}
*/
// error[E0308]: `if` and `else` have incompatible types



// Repetition with Loops
// loop, while and for.

// Repeating Code with loop
// The `loop` keyword tells Rust to execute a block of code over and over again forever,
// until you explicitly tell it to stop.
fn loop_main() {
    loop {
        println!("again!");
    }
}

// `break` keyword within the loop to tell the program when to stop executing the loop.
// `continue` keyword in a loop tells the program to skip over any remaining code in this iteration of the loop,
// and go to the next iteration.


// Returning Values from Loops

// pass the result of out of loop to others,
// can add the value want returned after the `break` expression.
// that value will be returned out of the loop:
fn add_value_after_break_from_loop_main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {result}");
}
// after loop, the "counter" will "* 2".


// Loop Labels to Disambiguate Between Multiple Loops
// Loop labels can use with `break` or `continue`.
// Loop labels must begin with a single quote.
// Here is an example with two nested loops:
fn loop_labels_in_multiple_loops_main() {
    let mut count = 0;
    'counting_up: loop {
        println!("count = {count}");
        let mut remaining = 10;

        loop {
            println!("remaining = {remaining}");
            if remaining == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
        remaining -= 1;
        }

        count += 1;
    }
    println!("End count = {count}");
}


// Conditional Loops with while

// Old method is using combination of `loop`, `if`, `else` and `break`;
// new method is `while` loop.
fn while_loop_main() {
    let mut number = 3;

    while number != 0 {
        println!("{number}!");

        number -= 1
    }

    println!("LIFTOFF!!!");
}
// `while` eliminates a lot of nesting that would be necessary if you used `loop`, `if`, `else`, and `break`, and it's clear.


// Looping Through a Collection with for

// can use `while` construct to loop over the elements of a collection.
fn for_or_while_main() {
    let a = [10, 20, 30, 40, 50];
    let mut index = 0;

    while index < 5 {
        println!("the value is: {}", a[index]);

        index += 1;
    }
}

// However, this approach is error prone;
// program panic if the index value or test condition is incorrect.

// `for` loop is more concise alternative choice.
fn for_loop_main() {
    let a = [10, 20, 30, 40, 50];

    for element in a {
        println!("the value is: {element}");
    }
}

// Here's method using `rev`, to reverse the range:
fn for_reverse_loop_main() {
    for number in (1..4).rev() {
        println!("{number}!");
    }
    println!("LIFTOFF!!!");
}