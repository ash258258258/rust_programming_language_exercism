use std::collections::btree_set::Difference;
use std::i32;
use std;
use std::io::stdin;
fn main() {
    // Two data type subsets: scalar and compound.

    // example of *type annotation* in previous chapter:
    let guess: u32 = "42".parse().expect("Not a Number!");
    
    // if remove : u32, it will `error: could not compile `no_type_annotations` due to previous error`:
    // let guess = "42".parse().expect("Not a Number!");
    // run `rustc --explain E0282` to show detail error.


    // Scalar Types
    // four primary scalar types: 
    // integers, floating-point numbers, Booleans, and characters.


    // Integer Types
    // sized integer `i8,...,i128, isize`
    // unsized integer `u8,...u128, usize`
    // The primary situation in which you’d use `isize` or `usize` is when indexing some sort of collection.

    // Integer Overflow
    // out integer range, 
    // when compile in debug mode, it will panic at runtime.
    // when compile in release mode, Rust performs *two’s complement wrapping*. Not panic.
    // To explicitly handle the possibility of overflow, use these families of methods provided by the std lib for *primitive numeric types*:
    // Wrap in all modes with the `wrapping_*` methods.
    // Return the `None` value if there is overflow with the `checked_*` methods.
    // Return the value and a boolean indicatin whether there was overflow with the `overflowing_*` methods.
    // Saturate at the value's minimum or maximum values with the `saturating_*` methods.


    // Floating-Point types
    // f32, f64, all of them are signed.
    let x = 2.0; // f64
    let y: f32 =3.0; // f32


    // Numeric Operations
    // mathematical operations in a `let` statement:
    // addition
    let sum = 5 + 10;
    
    // subtraction
    let difference = 95.5 - 4.3;

    // multiplication
    let product = 4 * 30;

    // division
    let quotient = 56.7 / 32.2;
    let truncated = -5 / 3; // Results in -1

    // remainder
    let remainder = 43 % 5;


    // The Boolean Type
    // `true` and `false`, 1 byte in size.
    // Boolean type using `bool`.
    let t = true;

    let f: bool = false; // with explicit type annotation


    // The Character type
    let c = 'z';
    let z: char = 'Z'; // with explicit type annotation
    let heart_eyed_cat = '😻';



    // Compound Types

    // Compound types can group multiple values into one type.
    // tuples and arrays


    // The Typle Type
    // A tuple is a general way of grouping together a number of values with a *variety* of types into one compound type.
    // Tuples have a fixed length: once declared, they cannot grow or shrink in size.
    let tup: (i32, f64, u8) = (500, 6.4, 1);

    // also can declare tuple without type annotations
    // To get the individual values out of a tuple, we can use pattern matching to *destructure* a tuple value:
    let tup = (500, 6.4, 1);
    
    let (x, y, z) = tup;
    
    println!("The value of y is: {y}");

    // We can also access a tuple element directly by using a period (`.`) followed by the index of the value:
    let x:(i32, f64, u8) = (500, 6.4, 1);
    
    let five_hundred = x.0;

    let six_point_four = x.1;

    let one = x.2;

    // empty tuple a.k.a. unit.
    // This value and its corresponding type are both written `()`
    // and represent an empty value or an empty return type.
    // Expressions implicitly return the unit value, if they don’t return any other value.


    // The Array Type
    // every element of an array must have the same type.
    // arrays in Rust have a fixed length.
    let a = [1, 2, 3, 4, 5];

    // A vector is a *similar collection type* provided by the standard library that is allowed to grow or shrink in size.
    let mouths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // the type annotation of arrays:
    let a: [i32; 5] = [1, 2, 3, 4, 5];

    // You can also initialize an array to contain the same value for each element by specifying the initial value:
    let a = [3; 5]; // a = [3, 3, 3, 3, 3];

    // Accessing Array Elements
    let a = [1, 2, 3, 4, 5];

    let first = a[0];
    let second = a[1];

    // Invalid Array Element Access
    /*
use std::io;

fn main() {
    let a = [1, 2, 3, 4, 5];

    println("Please enter an array index.");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index
        .trim()
        .parse()
        .expect("Index entered was not a number");

    let element = a[index];

    println!("The value of the element at index {index} is: {element}");
}
     */
    // This code compiles successfully.
    // the code can correct if you `cargo run`,
    // and enter 0,1,2,3,4.
    // But if you enter such as 10, see output:
    //thread 'main' panicked at 'index out of bounds: the len is 5 but the index is 10', src/main.rs:19:19
    //note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

}   
