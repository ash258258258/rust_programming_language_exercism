// Parameters(arguments)
// In function signatures, you must declare the type of each parameter.
fn main() {
    print_lableled_measurement(5, 'h');

    statements_and_expressions();
}

fn print_lableled_measurement(value: i32, unit_label: char) {
    println!("The mesurement is: {value}{unit_label}");
}


// Statements and Expreesions
// the `6` in the statement `let y = 6;` is an expression that evaluates to the value `6`.
// Calling a function is an expression. 
// Calling a macro is an expression.
// A new scope block created with curly brackets is an expression.
fn statements_and_expressions() {
    let y = {
        let x = 3;
        x + 1
    };
    println!("The value of y is: {y}");
}
// Expressions do not include ending semicolons(`;`).
// If you add a semicolon to the end of an expression, you turn it into a statement, and it will then not return a value.


// Functions with Return Values
// we must declare their type after an arrow (`->`).
// *the return value of the function* is synonymous with *the value of the final expression in the block of the body of a function*.
// can return early from a function by using the return keyword and specifying a value,
// most functions return the last expression implicitly.
fn five() -> i32 {
    5
}

fn FwRV_main() {
    let x = five();

    println!("The value of x is: {x}");
}
// another example:
fn another_main() {
    let x = plus_one(5);

    println!("The value of x is: {x}");
}

fn plus_one(x: i32) -> i32 {
    x + 1
    // if changed, get a error:
    // x + 1;
    // error[E0308]: mismatched types
    // because expression return (), a.k.a. unit type, 
    // but function needs return i32.
}